extends StaticBody2D
class_name GenericNpc

enum NpcState {IDLE, IN_DIALOG}
enum NpcQuest {BATTLE, HINT}

@export var npc_quest: NpcQuest
@export var dialogue: Dialogue
@export var dialogue_manager: DialogueManager
@export var interactable_area: Interactable
@export var anim_tree: AnimationTree

var npc_state: NpcState = NpcState.IDLE
var timer: float = 0.0
var turn_time: float = randf_range(1, 3.5)

@onready var anim_state: AnimationNodeStateMachinePlayback = anim_tree.get("parameters/playback")

func _ready():
	interactable_area.player_interacted.connect(_on_player_interacted)
	dialogue_manager.dialog_started.connect(_on_dialog_started)
	dialogue_manager.dialog_finished.connect(_on_dialogue_finished)

func _physics_process(delta):
	match(npc_state):
		NpcState.IDLE:
			_npc_idle(delta)
		NpcState.IN_DIALOG:
			_npc_in_dialog(delta)

func _on_player_interacted(player: Player) -> void:
	print("Interact with ", self.name)
	npc_state = NpcState.IN_DIALOG
	var direction: Vector2 = Vector2()
	match(player.facing_direction):
		Player.FacingDirection.LEFT:
			direction = Vector2.RIGHT
		Player.FacingDirection.RIGHT:
			direction = Vector2.LEFT
		Player.FacingDirection.UP:
			direction = Vector2.DOWN
		Player.FacingDirection.DOWN:
			direction = Vector2.UP
	anim_state.travel("Turn")
	anim_tree.set("parameters/Idle/blend_position", direction)
	anim_tree.set("parameters/Walk/blend_position", direction)
	anim_tree.set("parameters/Turn/blend_position", direction)
	
	dialogue_manager.dialog_started.emit(dialogue)

func _npc_idle(delta: float) -> void:
	var direction: Vector2 = Vector2(randi_range(-1, 1), randi_range(-1, 1))
	if (timer > turn_time):
		anim_state.travel("Turn")
		anim_tree.set("parameters/Idle/blend_position", direction)
		anim_tree.set("parameters/Walk/blend_position", direction)
		anim_tree.set("parameters/Turn/blend_position", direction)
		timer = 0
	timer += delta

func _npc_in_dialog(_delta: float) -> void:
	pass

func _on_dialog_started(_dialogue: Dialogue):
	npc_state = NpcState.IN_DIALOG

func _on_dialogue_finished():
	npc_state = NpcState.IDLE
	match npc_quest:
		NpcQuest.HINT:
			pass
		NpcQuest.BATTLE:
			print("Battle with %s started!" % self.name)
