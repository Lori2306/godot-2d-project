extends Area2D
class_name Door

@export var next_scene_path: String
@export var is_invisible: bool

@export var spawn_location: Vector2
@export var spawn_direction: Vector2

@onready var sprite = $Sprite2D
@onready var anim_player = $AnimationPlayer

var player_entered: bool

func _ready():
	if is_invisible:
		sprite.texture = null
	var player: Player = Utils.get_player()
	player.entering_door.connect(open_door)
	player.entered_door.connect(close_door)
	
func open_door():
	if player_entered:
		anim_player.play("open_door")

func close_door():
	if player_entered:
		anim_player.play("close_door")

func door_closed():
	if player_entered:
		Utils.get_scene_manager().transition_to_scene(next_scene_path, spawn_location, spawn_direction)

func _on_body_entered(body):
	player_entered = true


func _on_body_exited(body):
	player_entered = false
