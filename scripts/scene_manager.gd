extends Node2D
class_name SceneManager


func transition_to_scene(next_scene: String, player_position: Vector2, player_direction: Vector2):
	$CurrentScene.get_child(0).queue_free()
	$CurrentScene.add_child(load(next_scene).instantiate())
	var player: Player = Utils.get_player()
	player.set_spawn(player_position, player_direction)
