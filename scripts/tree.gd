extends StaticBody2D

enum TreeTipology {SPRING, FALL}

@export var type: TreeTipology
@onready var sprite: Sprite2D = $Sprite2D

var spring_indexes: Array = [0,2,3,5]
var fall_indexes: Array = [1,4]

func _ready():
	match type:
		TreeTipology.SPRING:
			sprite.frame = spring_indexes.pick_random()
		TreeTipology.FALL:
			sprite.frame = fall_indexes.pick_random()
