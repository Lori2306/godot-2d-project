extends CharacterBody2D
class_name Player

signal entering_door
signal entered_door

enum PlayerState {IDLE, WALK, TURN, IN_DIALOG, EXIT_DIALOGUE, ENTERING_DOOR}
enum FacingDirection {LEFT, RIGHT, UP, DOWN}

@export var SPEED = 4.0
@export var TILE_SIZE: int = 16
@export var START_POSITION: Vector2 = Vector2(20,20)

@export var anim_tree: AnimationTree
@export var collision_raycast: RayCast2D
@export var interaction_raycast: RayCast2D
@export var door_raycast: RayCast2D

@export var dialogue_manager: DialogueManager

@onready var anim_state: AnimationNodeStateMachinePlayback = anim_tree.get("parameters/playback")

var player_state: PlayerState = PlayerState.IDLE
var facing_direction: FacingDirection = FacingDirection.DOWN

var percent_moved_to_next_tile : float = 0.0
var initial_position: Vector2 = Vector2.ZERO
var input_direction: Vector2 = Vector2(0, 1)

func _ready():
	position.x = START_POSITION.x
	position.y = START_POSITION.y
	initial_position = position
	anim_tree.set("parameters/Idle/blend_position", input_direction)
	anim_tree.set("parameters/Walk/blend_position", input_direction)
	anim_tree.set("parameters/Turn/blend_position", input_direction)
	anim_tree.active = true
	
	dialogue_manager.dialog_started.connect(_on_dialogue_started)
	dialogue_manager.dialog_finished.connect(_on_dialogue_finished)

func _physics_process(delta):
	match player_state:
		PlayerState.IDLE:
			_player_idle(delta)
		PlayerState.WALK:
			_player_walk(delta)
		PlayerState.TURN:
			_player_turn(delta)
		PlayerState.IN_DIALOG:
			_player_in_dialog(delta)
		PlayerState.EXIT_DIALOGUE:
			player_state = PlayerState.IDLE
		PlayerState.ENTERING_DOOR:
			_player_entering_door(delta)

func set_spawn(location: Vector2, direction: Vector2):
	position = location
	anim_tree.set("parameters/Idle/blend_position", direction)
	anim_tree.set("parameters/Walk/blend_position", direction)
	anim_tree.set("parameters/Turn/blend_position", direction)

func _player_idle(_delta: float) -> void:
	if input_direction.y == 0:
		input_direction.x = int(Input.is_action_pressed("right")) - int(Input.is_action_pressed("left"))
	if input_direction.x == 0:
		input_direction.y = int(Input.is_action_pressed("down")) - int(Input.is_action_pressed("up"))
	if input_direction.length() > 0:
		anim_tree.set("parameters/Idle/blend_position", input_direction)
		anim_tree.set("parameters/Walk/blend_position", input_direction)
		anim_tree.set("parameters/Turn/blend_position", input_direction)
		if _need_to_turn():
			player_state = PlayerState.TURN
			anim_state.travel("Turn")
			print("Change to TURN")
		else:
			initial_position = position
			player_state = PlayerState.WALK
			anim_state.travel("Walk")
			print("Change to WALK")
	elif Input.is_action_just_pressed("interact"):
		_player_interact()

func _player_walk(delta: float) -> void:
	var desired_step: Vector2 = input_direction * TILE_SIZE / 2
	collision_raycast.target_position = desired_step
	collision_raycast.force_raycast_update()
	door_raycast.force_raycast_update()
	
	interaction_raycast.target_position = desired_step
	door_raycast.target_position = desired_step
	
	if door_raycast.is_colliding():
		if percent_moved_to_next_tile == 0.0:
			$Sprite2D.z_index = 2
			entering_door.emit()
			player_state = PlayerState.ENTERING_DOOR
			print("Change to ENTERING_DOOR")
			return
	
	if collision_raycast.is_colliding():
		# Torno in IDLE per gestire le collisioni
		percent_moved_to_next_tile = 0
		player_state = PlayerState.IDLE
		anim_state.travel("Idle")
		print("Change to IDLE")
	else:
		percent_moved_to_next_tile += SPEED * delta
		if percent_moved_to_next_tile >= 1.0:
			position = initial_position + (input_direction * TILE_SIZE)
			percent_moved_to_next_tile = 0.0
			match input_direction:
				Vector2.DOWN:
					if not Input.is_action_pressed("down"):
						player_state = PlayerState.IDLE
						anim_state.travel("Idle")
						print("Change to IDLE")
					else:
						initial_position = position
				Vector2.UP:
					if not Input.is_action_pressed("up"):
						player_state = PlayerState.IDLE
						anim_state.travel("Idle")
						print("Change to IDLE")
					else:
						initial_position = position
				Vector2.RIGHT:
					if not Input.is_action_pressed("right"):
						player_state = PlayerState.IDLE
						anim_state.travel("Idle")
						print("Change to IDLE")
					else:
						initial_position = position
				Vector2.LEFT:
					if not Input.is_action_pressed("left"):
						player_state = PlayerState.IDLE
						anim_state.travel("Idle")
						print("Change to IDLE")
					else:
						initial_position = position
		else:
			position = initial_position + (input_direction * TILE_SIZE * percent_moved_to_next_tile)

func _player_turn(_delta: float) -> void:
	var desired_step: Vector2 = input_direction * TILE_SIZE / 2
	interaction_raycast.target_position = desired_step
	collision_raycast.target_position = desired_step
	door_raycast.target_position = desired_step

func _finish_turn() -> void:
	player_state = PlayerState.IDLE
	print("Change to IDLE")

func _need_to_turn():
	var new_facing_direction: FacingDirection = FacingDirection.DOWN
	if input_direction.x < 0:
		new_facing_direction = FacingDirection.LEFT
	elif input_direction.x > 0:
		new_facing_direction = FacingDirection.RIGHT
	elif input_direction.y < 0:
		new_facing_direction = FacingDirection.UP
	elif input_direction.y > 0:
		new_facing_direction = FacingDirection.DOWN
	
	if facing_direction != new_facing_direction:
		facing_direction = new_facing_direction
		return true
	facing_direction = new_facing_direction
	return false

func _player_interact() -> void:
	interaction_raycast.force_raycast_update()
	if interaction_raycast.is_colliding():
		var interactable_object: Interactable = interaction_raycast.get_collider()
		if interactable_object:
			interactable_object.interact(self)
		else:
			print("Any interactable objects in the nearby")


func _player_in_dialog(_delta: float) -> void:
	pass

func _on_dialogue_started(_dialogue: Dialogue) -> void:
	player_state = PlayerState.IN_DIALOG

func _on_dialogue_finished() -> void:
	player_state = PlayerState.EXIT_DIALOGUE

func _player_entering_door(delta: float) -> void:
	percent_moved_to_next_tile += SPEED * delta
	if percent_moved_to_next_tile >= 1.0:
		position = initial_position + (input_direction * TILE_SIZE)
		percent_moved_to_next_tile = 0.0
		player_state = PlayerState.IDLE
		anim_state.travel("Idle")
		print("Change to IDLE")
		entered_door.emit()
		$Sprite2D.z_index = 0
	else:
		position = initial_position + (input_direction * TILE_SIZE * percent_moved_to_next_tile)

