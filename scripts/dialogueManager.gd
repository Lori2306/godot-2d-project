extends CanvasLayer
class_name DialogueManager

enum DialogueState {OFF, ON_DIALOGUE}

signal dialog_finished
signal dialog_started(dialogue)

@export var dialogue_box: NinePatchRect
@export var name_text: RichTextLabel
@export var chat_text: RichTextLabel
@export var audio_player: AudioStreamPlayer

var current_dialogue: Dialogue
var current_dialogue_id: int
var state: DialogueState

func _ready():
	self.dialog_started.connect(_on_dialogue_started)
	self.dialog_finished.connect(_on_dialogue_finished)
	dialogue_box.visible = false

func _input(event):
	if event.is_action_pressed("interact"):
		if state == DialogueState.ON_DIALOGUE:
			next_dialogue()

func next_dialogue():
	current_dialogue_id += 1
	if current_dialogue_id >= current_dialogue.dialogue_panels.size():
		self.dialog_finished.emit()
		return
	audio_player.playing = true
	name_text.text = current_dialogue.dialogue_name
	chat_text.text = current_dialogue.dialogue_panels[current_dialogue_id]


func _on_dialogue_started(dialogue: Dialogue) -> void:
	current_dialogue = dialogue
	current_dialogue_id = -1
	
	state = DialogueState.ON_DIALOGUE
	next_dialogue()
	dialogue_box.visible = true


func _on_dialogue_finished() -> void:
	state = DialogueState.OFF
	dialogue_box.visible = false
