extends Area2D
class_name Interactable

signal player_interacted(player)


func interact(player: Player) -> void:
	player_interacted.emit(player)
